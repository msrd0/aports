# Contributor: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
# Maintainer: Kiyoshi Aman <kiyoshi.aman+adelie@gmail.com>
pkgname=nheko
pkgver=0.6.4
pkgrel=0
pkgdesc="Qt5-based client for Matrix protocol"
url="https://github.com/Nheko-Reborn/nheko/releases"
arch="all !armhf" # Limited by qt5-qtmultimedia
license="GPL-3.0-or-later"
options="!check" # no test suite
makedepends="
	cmake
	ninja
	boost-dev
	lmdb-dev
	lmdbxx
	tweeny
	qt5-qtbase-dev
	qt5-qttools-dev
	qt5-qtmultimedia-dev
	qt5-qtsvg-dev
	zlib-dev
	openssl-dev
	olm-dev
	nlohmann-json
	cmark-dev
	spdlog-dev
	mtxclient-dev
	libsodium-dev
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/Nheko-Reborn/nheko/archive/v$pkgver.tar.gz
	modern-spdlog.patch
	"


prepare() {
	default_prepare
	sed -e '/-Wall/d' \
		-e '/-Wextra/d' \
		-e '/-Werror/d' \
		-e '/-pedantic/d' \
		-e '/-pipe/d' \
		-i CMakeLists.txt
}

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake -B build \
		-GNinja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_SKIP_RPATH=True \
		-DCMAKE_BUILD_TYPE=None \
		$CMAKE_CROSSOPTS .
	ninja -C build
}

check() {
	ninja -C build check
}

package() {
	DESTDIR="$pkgdir" ninja -C build install
}

sha512sums="782673a3ae642029307517798e5be96cf0473cd4261af891988a35dfeb5e2e724d2c034407f62addd2e15973d287d11c6590d70cca739c705354cb0151536470  nheko-0.6.4.tar.gz
0552de80a9ce1add4ec852c376ddc0d8cb326b3c78d83d6bc4f98a4fd1a13ee4e04f322f7bbc629eafb917b4072cc9986756d31ca1e0152875060a01c2896d1b  modern-spdlog.patch"
